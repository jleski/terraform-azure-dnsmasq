variable "ssh_key_name" {
  type = string
}

variable "ssh_key_resourcegroup" {
  type = string
}

data "azurerm_ssh_public_key" "vm_ssh_key" {
  name                = var.ssh_key_name
  resource_group_name = var.ssh_key_resourcegroup
}
