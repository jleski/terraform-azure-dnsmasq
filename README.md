# terraform-azure-dnsmasq

Terraform code to install small DNS resolver in Azure.

## Description

This deployment will:

* install one SUSE Linux Enterprise Server 15 SP3 VM to existing Azure Resource Group, Virtual Network and Subnet with Private IP assignment only.
* configure dnsmasq using cloud-init

## Requirements

* Azure subscription
* Azure Resource Group
* Azure Virtual Network
* Azure Subnet
* SSH Public Key in Azure
* Region where SLES SKUs are available
* Network connectivity to the hosts using Private IPs

## Usage

```bash
$ cp terraform.tfvars.example terraform.tfvars
# make adjustments to terraform.tfvars
$ terraform init
$ terraform apply -refresh -auto-approve
```
