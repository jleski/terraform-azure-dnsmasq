variable "vm_admin_username" {
  type = string
}

variable "vm_size" {
  type = string
}

variable "vm_resourcegroup" {
  type = string
}

variable "vm_subnet" {
  type = string
}

variable "vm_vnet" {
  type = string
}

variable "vm_vnet_resourcegroup" {
  type = string
}

data "azurerm_resource_group" "vm_resourcegroup" {
  name = var.vm_resourcegroup
}

data "azurerm_resource_group" "vm_vnet_resourcegroup" {
  name = var.vm_vnet_resourcegroup
}

data "azurerm_virtual_network" "vm_vnet" {
  name                = var.vm_vnet
  resource_group_name = var.vm_vnet_resourcegroup
}

data "azurerm_subnet" "vm_subnet" {
  name                 = var.vm_subnet
  virtual_network_name = var.vm_vnet
  resource_group_name  = var.vm_vnet_resourcegroup
}

resource "azurerm_network_interface" "vm_resolver_1_nic" {
  name                = "vm-resolver-1-nic"
  location            = data.azurerm_resource_group.vm_resourcegroup.location
  resource_group_name = data.azurerm_resource_group.vm_resourcegroup.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = data.azurerm_subnet.vm_subnet.id
    private_ip_address_allocation = "Dynamic"
  }
}

data "template_file" "user_data" {
  template = file("cloudconfig.yaml")
}

resource "azurerm_linux_virtual_machine" "vm_resolver_1" {
  name                  = "vm-resolver-1"
  location              = data.azurerm_resource_group.vm_resourcegroup.location
  resource_group_name   = data.azurerm_resource_group.vm_resourcegroup.name
  network_interface_ids = [azurerm_network_interface.vm_resolver_1_nic.id]
  size                  = var.vm_size
  custom_data           = base64encode(data.template_file.user_data.rendered)

  os_disk {
    name                 = "vm-resolver-1-OsDisk"
    caching              = "ReadWrite"
    storage_account_type = "StandardSSD_LRS"
  }

  source_image_reference {
    publisher = "SUSE"
    offer     = "sles-15-sp3"
    sku       = "gen2"
    version   = "latest"
  }

  computer_name                   = "vm-resolver-1"
  admin_username                  = var.vm_admin_username
  disable_password_authentication = true

  admin_ssh_key {
    username   = var.vm_admin_username
    public_key = data.azurerm_ssh_public_key.vm_ssh_key.public_key
  }
}

output "vm_resolver_1_ip" {
  value = azurerm_linux_virtual_machine.vm_resolver_1.private_ip_address
}
